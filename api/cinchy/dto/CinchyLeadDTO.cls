public with sharing class CinchyLeadDTO extends MappedParamsWrapper {
    public String Created;
    public String CinchyId;
    public String Email;
    public String Phone;
    public String Company;
    public String Message;
    public String FirstName;
    public String LastName;
    public String JobTitle;
    public String MarketingURL;
    public String MarketingSource;
    public String PrimaryBrokerGuid;
    public String OtherBrokersGuid;
    public String PropertyID;

    private static final Map<String, String> EXTERNAL_TO_SF_MAPPING = new Map<String, String>{
        'Cinchy Id' => 'CinchyId',
        'First Name' => 'FirstName',
        'Last Name' => 'LastName'
    };

    public Map<String, String> getPropertyMappings() {
        return EXTERNAL_TO_SF_MAPPING;
    }
}
