@isTest
private class LeadCinchyIntegrationBatchTest {
    @isTest
    static void cinchyLeadsAreCreated() {
        Campaign campaign = new CampaignTestData()
            .save(SaveMethod.DATABASE)
            .get();

        OpportunityTestDataFactory.build(
            (new OpportunityTestData())
                .withOlListingNumber(CinchyCalloutResponsesMock.PROPERTY_ID.removeStartIgnoreCase('DNK'))
                .withName('OL Property ' + CinchyCalloutResponsesMock.PROPERTY_ID)
                .withPrimaryCampaignId(campaign.Id),
            SaveMethod.DATABASE
        );

        List<User> users = new List<User>();
        users.add(
          new UserTestData()
            .withBrokerNumberEJ(CinchyCalloutResponsesMock.PRIMARY_BROKER_GUID)
            .save(SaveMethod.NO_SAVE)
            .get()
        );
        users.add(
          new UserTestData()
            .withBrokerNumberEJ(CinchyCalloutResponsesMock.OTHER_BROKERS_GUID)
            .save(SaveMethod.NO_SAVE)
            .get()
        );
        insert users;

        Test.startTest();
        Test.setMock(
            HttpCalloutMock.class,
            new CinchyCalloutResponsesMock.CinchySuccessResponse()
        );

        new LeadCinchyIntegrationBatch().execute(null);
        Test.stopTest();

        System.assertEquals(2, LeadTestSelector.getCinchyLeadsCount());
        System.assertEquals(1, LogTestSelector.getAllLogsCount());
        List<Lead> cinchyLeads = LeadTestSelector.getCinchyLeads();
        for (Lead cinchyLead : cinchyLeads) {
            System.assertEquals(LeadConverter.DEFAULT_COUNTRY, cinchyLead.Country);
        }
    }

    @isTest
    static void cinchyLeadsAreNotCreatedWhenCinchyResponseIsNotOk() {
        Campaign campaign = new CampaignTestData()
            .save(SaveMethod.DATABASE)
            .get();

        OpportunityTestDataFactory.build(
            (new OpportunityTestData())
                .withOlListingNumber(CinchyCalloutResponsesMock.PROPERTY_ID.removeStartIgnoreCase('DNK'))
                .withName('OL Property ' + CinchyCalloutResponsesMock.PROPERTY_ID)
                .withPrimaryCampaignId(campaign.Id),
            SaveMethod.DATABASE
        );

        List<User> users = new List<User>();
        users.add(
          new UserTestData()
            .withBrokerNumberEJ(CinchyCalloutResponsesMock.PRIMARY_BROKER_GUID)
            .save(SaveMethod.NO_SAVE)
            .get()
        );
        users.add(
          new UserTestData()
            .withBrokerNumberEJ(CinchyCalloutResponsesMock.OTHER_BROKERS_GUID)
            .save(SaveMethod.NO_SAVE)
            .get()
        );
        insert users;

        Test.startTest();
        Test.setMock(
            HttpCalloutMock.class,
            new CinchyCalloutResponsesMock.CinchyFailureResponse()
        );

        new LeadCinchyIntegrationBatch().execute(null);
        Test.stopTest();

        System.assertEquals(0, LeadTestSelector.getCinchyLeadsCount());
        System.assertEquals(1, LogTestSelector.getAllLogsCount());
    }

    @isTest
    static void cinchyLeadsAreCreatedWithoutBrokerWhenBrokerWithGivenGuidDoesNotExist() {
        Campaign campaign = new CampaignTestData()
            .save(SaveMethod.DATABASE)
            .get();

        OpportunityTestDataFactory.build(
            (new OpportunityTestData())
                .withOlListingNumber(CinchyCalloutResponsesMock.PROPERTY_ID.removeStartIgnoreCase('DNK'))
                .withName('OL Property ' + CinchyCalloutResponsesMock.PROPERTY_ID)
                .withPrimaryCampaignId(campaign.Id),
            SaveMethod.DATABASE
        );

        (new UserTestData()).withBrokerNumberEJ(CinchyCalloutResponsesMock.OTHER_BROKERS_GUID).save(SaveMethod.DATABASE);

        Test.startTest();
        Test.setMock(
            HttpCalloutMock.class,
            new CinchyCalloutResponsesMock.CinchySuccessResponse()
        );

        new LeadCinchyIntegrationBatch().execute(null);
        Test.stopTest();

        System.assertEquals(2, LeadTestSelector.getCinchyLeadsCount());
        System.assertEquals(1, LogTestSelector.getAllLogsCount());
        List<Lead> cinchyLeads = LeadTestSelector.getCinchyLeads();
        for (Lead cinchyLead : cinchyLeads) {
            System.assertEquals(LeadConverter.DEFAULT_COUNTRY, cinchyLead.Country);
        }
    }

    @isTest
    static void cinchyLeadsAreCreatedWithoutOpportunityWhenOpportunityWithGivenOlListingNumberDoesNotExist() {
        List<User> users = new List<User>();
        users.add(
          new UserTestData()
            .withBrokerNumberEJ(CinchyCalloutResponsesMock.PRIMARY_BROKER_GUID)
            .save(SaveMethod.NO_SAVE)
            .get()
        );
        users.add(
          new UserTestData()
            .withBrokerNumberEJ(CinchyCalloutResponsesMock.OTHER_BROKERS_GUID)
            .save(SaveMethod.NO_SAVE)
            .get()
        );
        insert users;

        Test.startTest();
        Test.setMock(
            HttpCalloutMock.class,
            new CinchyCalloutResponsesMock.CinchySuccessResponse()
        );

        new LeadCinchyIntegrationBatch().execute(null);
        Test.stopTest();

        System.assertEquals(2, LeadTestSelector.getCinchyLeadsCount());
        System.assertEquals(1, LogTestSelector.getAllLogsCount());
        List<Lead> cinchyLeads = LeadTestSelector.getCinchyLeads();
        for (Lead cinchyLead : cinchyLeads) {
            System.assertEquals(LeadConverter.DEFAULT_COUNTRY, cinchyLead.Country);
        }
    }
}
