public with sharing class LeadConverter {
    private static final String DEFAULT_COMPANY_NAME = AppConfig.cinchyTechnicalAccountName;
    @TestVisible
    private static final String DEFAULT_COUNTRY = AppConfig.cinchyLeadDefaultCountry;
    private static final String LEAD_SOURCE = 'Colliers.com';
    private static final Id LEAD_RT_ID = RecordTypes.leadRT.potentialBuyersTenants.recordTypeId;
    private static final Map<String, Integer> MONTHS_MAP = new Map<String, Integer>{
        'Jan' => 1,
        'Feb' => 2,
        'Mar' => 3,
        'Apr' => 4,
        'May' => 5,
        'Jun' => 6,
        'Jul' => 7,
        'Aug' => 8,
        'Sep' => 9,
        'Oct' => 10,
        'Nov' => 11,
        'Dec' => 12
    };
    private static final Pattern DATETIME_PATTERN = Pattern.compile('(\\w{3})\\s{1}(\\d{1,2})\\,\\s{1}(\\d{4})\\s{1}(\\d{1,2})\\:(\\d{2})\\s{1}(\\w{2})'); //eg. Mar 17, 2021 2:42 PM

    public static List<Lead> convert(List<CinchyLeadDTO> leadsData) {
        CinchyRelationIdsCollector idsCollector = new CinchyRelationIdsCollector(leadsData);

        List<Lead> leads = new List<Lead>();
        for (CinchyLeadDTO leadData : leadsData) {
            if (idsCollector.hasCinchyId(leadData.CinchyId)) {
                continue;
            }

            String listingId = String.isNotBlank(leadData.PropertyID)
                ? leadData.PropertyID.removeStartIgnoreCase('DNK')
                : null;

            String primaryBrokerNumberEj = leadData.PrimaryBrokerGuid;
            String otherBrokerNumberEj = String.isNotBlank(leadData.OtherBrokersGuid)
                ? leadData.OtherBrokersGuid.split(StringUtility.SEMICOLON)[0]
                : StringUtility.EMPTY_STRING;

            String company = leadData.Company;
            String marketingUrl = leadData.MarketingURL;

            Lead lead = new Lead(
                CinchyCreatedDate__c = parseDatetime(leadData.Created),
                CinchyID__c = leadData.CinchyId,
                Country = DEFAULT_COUNTRY,
                Email = leadData.Email,
                Phone = leadData.Phone,
                Company = !String.isBlank(company) ? company : DEFAULT_COMPANY_NAME,
                CinchyMessage__c = leadData.Message,
                FirstName = leadData.FirstName,
                LastName = leadData.LastName,
                Title = leadData.JobTitle,
                MarketingURL__c = !String.isBlank(marketingUrl) ? marketingUrl.left(255) : null,
                MarketingSource__c = leadData.MarketingSource,
                ListingOpportunityId__c = idsCollector.getOpportunityIdByListingNumber(listingId),
                PrimaryBrokerID__c = idsCollector.getUserIdByBrokerEjNumber(primaryBrokerNumberEj),
                OtherBrokerID__c = idsCollector.getUserIdByBrokerEjNumber(otherBrokerNumberEj),
                LeadSource = LEAD_SOURCE,
                RecordTypeId = LEAD_RT_ID
            );

            leads.add(lead);
        }

        return leads;
    }

    private static Datetime parseDatetime(String datetimeString) {
        Matcher matcher = DATETIME_PATTERN.matcher(datetimeString);
        if (!matcher.matches()) {
            return null;
        }
        Integer month = MONTHS_MAP.get(matcher.group(1));
        Integer day = Integer.valueOf(matcher.group(2));
        Integer year = Integer.valueOf(matcher.group(3));
        Integer hour = Integer.valueOf(matcher.group(4));
        Integer minute = Integer.valueOf(matcher.group(5));
        String daypart = matcher.group(6);
        if (daypart == 'PM') {
            hour += 12;
        }

        return Datetime.newInstance(
            year,
            month,
            day,
            hour,
            minute,
            0
        );
    }
}
