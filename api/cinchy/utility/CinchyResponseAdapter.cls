public with sharing class CinchyResponseAdapter extends MappedJsonResponseAdapter {
    public List<CinchyLeadDTO> paramsWrapper = new List<CinchyLeadDTO>();

    public void resolve(HttpResponse originalResponse) {
        if (CalloutUtility.isSuccess(originalResponse.getStatusCode())) {
            List<Object> data = (List<Object>) JSON.deserializeUntyped(originalResponse.getBody());

            for (Object item : data) {
                this.paramsWrapper.add((CinchyLeadDTO) this.toReturnValue(JSON.serialize(item), CinchyLeadDTO.class));
            }
        }
    }
}
