public with sharing class CinchyIntegrationErrorEmailConfigurator implements IEmailConfigurator {
    private final String SUBJECT = Label.LeadCinchyIntegrationErrorEmailSubject;

    private List<String> errors;

    public CinchyIntegrationErrorEmailConfigurator(List<String> errors) {
        this.errors = errors;
    }

    public List<Messaging.SingleEmailMessage> prepareMessages() {
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setToAddresses(AppConfig.errorEmailAddresses);
        message.setSaveAsActivity(false);
        message.setSubject(SUBJECT);
        message.setPlainTextBody(
            String.join(this.errors, StringUtility.NEW_LINE)
        );
        return new List<Messaging.SingleEmailMessage>{ message };
    }

    public void performPostSendingActions(Set<Id> successWhatIds) {}
}
