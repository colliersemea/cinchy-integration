public with sharing class CinchyRelationIdsCollector {
    private Set<String> cinchyIds;
    private Map<String, Id> olListingNumberToOppIdMap;
    private Map<String, Id> brokerEjNumberToUserIdMap;

    public CinchyRelationIdsCollector(List<CinchyLeadDTO> leadsData) {
        this.collectIds(leadsData);
    }

    public Boolean hasCinchyId(String cinchyId) {
        return this.cinchyIds.contains(cinchyId);
    }

    public Id getOpportunityIdByListingNumber(String listingNumber) {
        if (String.isBlank(listingNumber)) {
            return null;
        }

        return this.olListingNumberToOppIdMap.get(listingNumber);
    }

    public Id getUserIdByBrokerEjNumber(String brokerEjNumber) {
        if (String.isBlank(brokerEjNumber)) {
            return null;
        }

        return this.brokerEjNumberToUserIdMap.get(brokerEjNumber);
    }

    private void collectIds(List<CinchyLeadDTO> leadsData) {
        Set<String> ids = new Set<String>();
        Set<String> brokerEjNumbers = new Set<String>();
        Set<String> listingIds = new Set<String>();

        for (CinchyLeadDTO leadData : leadsData) {
            String cinchyId = leadData.CinchyId;
            String primaryBrokerNumberEj = leadData.PrimaryBrokerGuid;
            String otherBrokersNumberEj = leadData.OtherBrokersGuid;
            Set<String> otherBrokersNumberEjs = !String.isBlank(otherBrokersNumberEj)
                ? new Set<String>(otherBrokersNumberEj.split(StringUtility.SEMICOLON))
                : new Set<String>();
            String listingId = leadData.PropertyID;
            listingId = !String.isBlank(listingId)
                ? listingId.removeStartIgnoreCase('DNK')
                : null;

            if (!String.isEmpty(cinchyId)) {
                ids.add(cinchyId);
            }
            if (!String.isEmpty(primaryBrokerNumberEj)) {
                brokerEjNumbers.add(primaryBrokerNumberEj);
            }
            if (!otherBrokersNumberEjs.isEmpty()) {
                brokerEjNumbers.addAll(otherBrokersNumberEjs);
            }
            if (!String.isBlank(listingId)) {
                listingIds.add(listingId);
            }
        }

        this.cinchyIds = this.getExistingLeadCinchyIds(ids);
        this.olListingNumberToOppIdMap = this.getOlListingNumberToOppIdMap(listingIds);
        this.brokerEjNumberToUserIdMap = this.getBrokerEjNumberToUserIdMap(listingIds);
    }

    private Set<String> getExistingLeadCinchyIds(Set<String> cinchyIds) {
        Set<String> existingCinchyIds = new Set<String>();
        List<Lead> leads = Selectors.leads.getByCinchyId(cinchyIds);

        for (Lead lead : leads) {
            existingCinchyIds.add(lead.CinchyID__c);
        }

        return existingCinchyIds;
    }

    private Map<String, Id> getOlListingNumberToOppIdMap(Set<String> listingNumbers) {
        Map<String, Id> olListingNumberToOppIdMap = new Map<String, Id>();
        List<Opportunity> opps = Selectors.opportunities.getByOlListingNumber(listingNumbers);

        for (Opportunity opp : opps) {
            olListingNumberToOppIdMap.put(opp.OL_ListingNumber__c, opp.Id);
        }

        return olListingNumberToOppIdMap;
    }

    private Map<String, Id> getBrokerEjNumberToUserIdMap(Set<String> brokerEjNumbers) {
        Map<String, Id> brokerEjNumberToUserIdMap = new Map<String, Id>();
        List<User> users = Selectors.users.getByBrokerNumberEj(brokerEjNumbers);

        for (User user : users) {
            brokerEjNumberToUserIdMap.put(user.BrokerNumberEJ__c, user.Id);
        }

        return brokerEjNumberToUserIdMap;
    }
}
