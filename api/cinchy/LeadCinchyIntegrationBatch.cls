public with sharing class LeadCinchyIntegrationBatch implements
    Database.Batchable<Lead>,
    Database.AllowsCallouts,
    Database.Stateful,
    Schedulable
{
    public static final String CRON_EXP = '0 0 4 * * ?'; //Daily at 4:00 AM
    public static final String JOB_NAME = 'Cinchy Last Leads Integration';

    private List<String> errors;
    private SObjectUnitOfWork unitOfWork;

    public LeadCinchyIntegrationBatch() {
        DML dml = new DML();
        dml.setAllowDuplicateSave();

        this.unitOfWork = new SObjectUnitOfWork(dml);
    }

    public LeadCinchyIntegrationBatch(SObjectUnitOfWork unitOfWork) {
        this.unitOfWork = unitOfWork;
        this.errors = new List<String>();
    }

    public void execute(SchedulableContext sc) {
        Database.executeBatch(
            new LeadCinchyIntegrationBatch(this.unitOfWork),
            10
        );
    }

    public void execute(Database.BatchableContext BC, List<Lead> leads) {
        try {
            this.unitOfWork.registerNew(leads);
            this.unitOfWork.commitWork();
        } catch (Exception ex) {
            this.handleException(ex);
        }
    }

    public List<Lead> start(Database.BatchableContext BC) {
        try {
            IntegrationController.Result result = CinchyIntegrationController.getLastLeads();

            List<Lead> leads = (List<Lead>) JSON.deserializeStrict(result.message, List<Lead>.class);

            return leads;
        } catch (Exception ex) {
            this.handleException(ex);

            return new List<Lead>();
        }
    }

    public void finish(Database.BatchableContext BC) {
        if (this.errors.isEmpty()) {
            return;
        }
        System.enqueueJob(
            Utils.getEmailService(
                new CinchyIntegrationErrorEmailConfigurator(this.errors)
            )
        );
    }

    private void handleException(Exception ex) {
        this.errors.add(ex.getMessage());
        new Logger(Picklists.logPicklists.areaCinchy.getValue()).log(ex);
    }
}
