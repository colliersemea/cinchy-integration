public with sharing class CinchyGetLastLeadsRequest {
    public List<CinchyLeadDTO> process() {
        HttpRequest request = new CinchyRequestBuilder()
            .setEndpoint(NamedCredentials.CINCHY)
            .setMethod(CalloutUtility.GET_METHOD)
            .build();

        CinchyResponseAdapter responseAdapter = new CinchyResponseAdapter();

        new HttpRequestSender(
            responseAdapter,
            new Logger(Picklists.logPicklists.areaCinchy.getValue())
        ).send(request);

        return responseAdapter.paramsWrapper;
    }
}
