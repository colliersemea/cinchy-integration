public with sharing class CinchyIntegrationService {
    public List<Lead> getLastDeals() {
        CinchyGetLastLeadsRequest request = new CinchyGetLastLeadsRequest();
        List<CinchyLeadDTO> leadsData = request.process();

        return LeadConverter.convert(leadsData);
    }
}
