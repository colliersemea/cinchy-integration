@isTest
public with sharing class CinchyCalloutResponsesMock {
    public static final String PROPERTY_ID = 'DNK123456789';
    public static final String PRIMARY_BROKER_GUID = 'F3593A86-85B2-4CA9-9230-AAE290FE43FA';
    public static final String OTHER_BROKERS_GUID = '93BADF84-6F7D-4F3B-8ED4-2A45C1B83EB1';
    public static final String CINCHY_RESPONSE = JSON.serialize(
        new List<Map<String, Object>>{
            new Map<String, Object>{
                'Cinchy Id' => 1,
                'Created' => 'Mar 17, 2021 10:43 AM',
                'Email' => '1fake1@email.com',
                'Phone' => '555 545 555',
                'Company' => '1Test Company1',
                'JobTitle' => 'Director',
                'MarketingURL' => 'www.google.com',
                'MarketingSource' => 'google',
                'Message' => 'Lorem ipsum',
                'First Name' => 'John',
                'Last Name' => 'Doe',
                'PropertyID' => PROPERTY_ID,
                'PrimaryBrokerGuid' => PRIMARY_BROKER_GUID,
                'OtherBrokersGuid' => OTHER_BROKERS_GUID
            },
            new Map<String, Object>{
                'Cinchy Id' => 2,
                'Created' => 'Jan 1, 2019 10:43 AM',
                'Email' => '2fake2@email.com',
                'Phone' => '545 555 545',
                'Company' => '2Test Company2',
                'JobTitle' => 'Manager',
                'MarketingURL' => 'www.google.com',
                'MarketingSource' => 'google',
                'Message' => 'Sit amet',
                'First Name' => 'Richard',
                'Last Name' => 'Feynmann',
                'PropertyID' => PROPERTY_ID,
                'PrimaryBrokerGuid' => PRIMARY_BROKER_GUID,
                'OtherBrokersGuid' => OTHER_BROKERS_GUID
            }
        }
    );

    public class CinchySuccessResponse implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(CalloutUtility.HTTP_RESPONSE_STATUS_CODE_200);
            response.setBody(CINCHY_RESPONSE);
            return response;
        }
    }

    public class CinchyFailureResponse implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(CalloutUtility.HTTP_RESPONSE_STATUS_CODE_500);
            response.setBody(StringUtility.EMPTY_STRING);
            return response;
        }
    }
}
