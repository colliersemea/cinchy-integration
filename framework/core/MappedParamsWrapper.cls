public abstract class MappedParamsWrapper implements ParamsWrapper, MappedProperties {
    public virtual Map<String, Object> toMap() {
        String serializedObject = JSON.serialize(this, true);
        Map<String, String> propertyMappings = ((MappedProperties) this).getPropertyMappings();
        for (String baseName : propertyMappings.keySet()) {
            serializedObject = serializedObject.replaceAll('"' + baseName + '"\\s*:',
                '"' + propertyMappings.get(baseName) + '":');
        }
        return (Map<String, Object>) JSON.deserializeUntyped(serializedObject);
    }
}
