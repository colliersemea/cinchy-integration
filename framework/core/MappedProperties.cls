public interface MappedProperties {
    Map<String, String> getPropertyMappings();
}
