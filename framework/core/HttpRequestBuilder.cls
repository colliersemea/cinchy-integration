public virtual class HttpRequestBuilder {
    private static final String ENDPOINT_FORMAT = '{0}?{1}';
    private static final String PARAMS_FORMAT = '{0}={1}';
    private static final String PARAMS_DELIMITER = StringUtility.AND_SIGN;

    private static final Integer TIMEOUT = CalloutUtility.TIMEOUT_DEFAULT;

    private AuthenticationMethod authMethod;
    protected HttpRequest request;
    protected HttpRequestBody body;
    protected Blob blobBody;

    private String endpoint;
    private Map<String, Object> paramsMap;

    public HttpRequestBuilder() {
        this.request = new HttpRequest();
        this.paramsMap = new Map<String, Object>();
    }

    public HttpRequestBuilder setAuthMethod(AuthenticationMethod authMethod) {
        this.authMethod = authMethod;
        return this;
    }

    public HttpRequestBuilder setMethod(String method) {
        this.request.setMethod(method);
        return this;
    }

    public HttpRequestBuilder setEndpoint(String endpoint) {
        this.endpoint = endpoint;
        return this;
    }

    public HttpRequestBuilder setParam(String key, Object value) {
        this.paramsMap.put(key, parseToUrlFormat(value));
        return this;
    }

    public HttpRequestBuilder setParams(ParamsWrapper paramsWrapper) {
        Map<String, Object> paramsMap = paramsWrapper.toMap();
        for(String key : paramsMap.keySet()) {
            this.paramsMap.put(key, parseToUrlFormat(paramsMap.get(key)));
        }
        return this;
    }

    public HttpRequestBuilder setHeader(String key, String value) {
        this.request.setHeader(key, value);
        return this;
    }

    public HttpRequestBuilder setBody(HttpRequestBody body) {
        this.body = body;
        return this;
    }

    public HttpRequestBuilder setBodyAsBlob(Blob body) {
        this.blobBody = body;
        return this;
    }

    public virtual HttpRequest build() {
        if(this.authMethod != null) {
            this.authMethod.setAuth(this);
        }
        if(this.endpoint != null) {
            this.request.setEndpoint(this.paramsMap.isEmpty() ? this.endpoint : buildEndpoint());
        }
        if(this.body != null) {
            this.request.setBody(this.body.toString());
        }
        if(this.blobBody != null) {
            this.request.setBodyAsBlob(this.blobBody);
        }
        this.request.setTimeout(TIMEOUT);
        return this.request;
    }

    private String buildEndpoint() {
        List<String> formattedParams = new List<String>();
        for(String key : this.paramsMap.keySet()) {
            formattedParams.add(
                String.format(
                    PARAMS_FORMAT,
                    new List<Object>{ key, this.paramsMap.get(key) }
            ));
        }
        return String.format(
            ENDPOINT_FORMAT,
            new List<Object> {
                this.endpoint,
                String.join(formattedParams, PARAMS_DELIMITER)
            });
    }

    private Object parseToUrlFormat(Object value) {
        return value instanceof String
            ? EncodingUtil.urlEncode((String) value, CalloutUtility.UTF_8)
            : value;
    }

    public HttpRequestBody getBody() {
        return this.body;
    }
}
