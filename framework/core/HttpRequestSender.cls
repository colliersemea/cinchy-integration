public class HttpRequestSender {
    private HttpResponseAdapter responseAdapter;
    private RequestLogger logger;

    public HttpRequestSender() {}

    public HttpRequestSender(HttpResponseAdapter responseAdapter) {
        this.responseAdapter = responseAdapter;
    }

    public HttpRequestSender(HttpResponseAdapter responseAdapter, RequestLogger logger) {
        this(responseAdapter);
        this.logger = logger;
    }

    public HttpResponse send(HttpRequest request) {
        Http http = new Http();
        HttpResponse response = http.send(request);
        this.logger?.log(request, response);
        this.responseAdapter?.resolve(response);
        return response;
    }
}
