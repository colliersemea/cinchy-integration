public interface ParamsWrapper {
    Map<String, Object> toMap();
}