public interface HttpResponseAdapter {
    void resolve(HttpResponse originalResponse);
}