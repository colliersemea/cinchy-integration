public abstract class MappedJsonResponseAdapter implements HttpResponseAdapter {
    protected Object toReturnValue(String body, Type returnType) {
        Object o = returnType.newInstance();
        if (o instanceof MappedProperties) {
            Map<String, String> propertyMappings = ((MappedProperties) o).getPropertyMappings();
            for (String baseName : propertyMappings.keySet()) {
                body = body.replaceAll('"' + baseName + '"\\s*:',
                    '"' + propertyMappings.get(baseName) + '":');
            }
        }
        JsonParser parser = Json.createParser(body);
        parser.nextToken();
        return parser.readValueAs(returnType);
    }
}
