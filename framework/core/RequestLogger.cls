public interface RequestLogger {
    void log(HttpRequest request, HttpResponse response);
}
