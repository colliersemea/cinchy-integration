public inherited sharing virtual class GenericHttpResponse implements HttpResponseAdapter {
    private final Logger logger;

    public Boolean isSuccess { get; private set; }
    public String error { get; protected set; }

    public GenericHttpResponse() {
        isSuccess = false;
    }

    public GenericHttpResponse(Logger logger) {
        this();
        this.logger = logger;
    }

    public void resolve(HttpResponse originalResponse) {
        if (CalloutUtility.isSuccess(originalResponse.getStatusCode())) {
            try {
                handleSuccess(originalResponse.getBody(), originalResponse.getBodyAsBlob());
                isSuccess = true;
            } catch (Exception exc) {
                if (logger != null) {
                    logger.log(exc);
                } else {
                    throw new GenericHttpResponseException(exc);
                }
            }
        } else {
            error = originalResponse.getBody();
            handleError(originalResponse.getBody(), originalResponse.getBodyAsBlob());
        }
    }

    protected virtual void handleSuccess(String body, Blob blobBody) {
    }

    protected virtual void handleError(String body, Blob blobBody) {
    }

    public class GenericHttpResponseException extends Exception {
    }
}
