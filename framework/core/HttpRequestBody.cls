public interface HttpRequestBody {
    HttpRequestBody addContents(Object contents);
    String toString();
}